# vigotech.github.io-nuxt

> VigoTech Alliance Website

## Build Setup

``` bash
# install dependencies
$ yarn install

# rename .env.example to .env and edit using your own config

# prepare date to generate website
$ yarn prepare

# serve with hot reload at localhost:3000
$ yarn run dev

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
